# Koui

<img title="" src=".gitlab/koui_header.png" alt="Koui Logo" data-align="inline" height="100px">

**Koui** is a very fast and flexible retained mode UI library for the [Kha](https://github.com/Kode/Kha) framework.

It's free and open source, but if you want to support me and my work, you can [buy it at Gumroad](https://gumroad.com/products/WQzWn).

![Screenshot](.gitlab/screenshot.png)

UI design provided by Sebastian Becker. Follow him on [Behance](https://www.behance.net/sebbeck) and [Instagram](https://www.instagram.com/sebastian_becker93/)!

## Getting started

See [Wiki/Getting Started](https://gitlab.com/koui/Koui/-/wikis/Getting-Started)

## Why yet another UI library?

I've created this library because I was missing a UI package for Kha that works well with game UIs. That's why Koui is targeted towards game UIs and not necessarily suited for desktop applications. 

While Zui (another UI library for Kha) is a great libary, it lacks support for customization, theming is very limited and it is more designed for desktop applications like [Armorpaint](https://github.com/armory3d/armorpaint) and [Armory2D](https://github.com/armory3d/armory2d). It is a little bit faster to setup a UI though, because Zui is an *immediate* mode library.

Koui's big strength is its **customizability**. You can change just everything and create
unique user interfaces very easily. You can even create your own UI elements
without much work. The API is designed to be as intuitive as possible without
taking too much freedom away from the user.

## Planned features

Some of the bigger features that are planned but not yet implemented:

- Animations

- Controller and touch support

- Better theme handling (no .json files anymore) and more theme properties (gradients, rounded corners etc.)

- Logic node pack for Armory3D

If you want to contribute, feel free to do so! Any help is highly appreciated!

## License

Koui is licensed under the [zlib license](LICENSE.md).

This project uses [Montserrat](https://github.com/JulietaUla/Montserrat) as the default font for the user interface, which is licensed under the [SIL Open Font License 1.1](https://gitlab.com/koui/Koui/-/blob/master/Assets/Montserrat-License-OFL.txt).

> If you don't agree with the font's license for your own projects, please remove that font and its license file from your local copy of Koui and remove the reference to it from Koui's `khafile.js`.
> 
> Koui will not work until you load in another font and reference it in the theme file.
