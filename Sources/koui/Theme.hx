package koui;

/**
 * This class contains the default theme.
 *
 * - To get an overview of what the different variables do in detail, please refer to
 *   [Wiki: Elements](https://gitlab.com/MoritzBrueckner/koui/-/wikis/Documentation/Elements).
 *
 * - To learn more about how tho create custom themes, please take a look at
 *   [Wiki: Custom Themes](https://gitlab.com/MoritzBrueckner/koui/-/wikis/Documentation/Customize/Custom-Themes).
 */
@:build(koui.utils.ThemeUtil.buildTheme())
class Theme {
}