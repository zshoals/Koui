package koui.events;

import koui.elements.Element;

/**
 * This typedef holds all information about a certain event. Each event has a
 * type and a state and – depending on its type – further attributes.
 *
 * @see [Wiki: Events](https://gitlab.com/koui/Koui/-/wikis/Documentation/Events)
 */
typedef Event = {
	/**
	 * The type of the event.
	 */
	final type: EventType;
	/**
	 * The state of the event.
	 */
	final state: EventState;

	/**
	 * The element that receives this event. Although marked as optional, this
	 * attribute does always exist.
	 */
	@:optional var element: Element;

	/**
	 * `true` if the mouse was moved in the last frame.
	 * @see `EventType.MouseHover`
	 */
	@:optional final mouseMoved: Bool;
	/**
	 * The mouse button that triggered the event.
	 * @see `EventType.Focus`
	 * @see `EventType.MouseClick`
	 */
	@:optional final mouseButton: MouseButton;
	/**
	 * The amount of mouse wheel rotation.
	 * @see `EventType.MouseScroll`
	 */
	@:optional final scrollDelta: Int;
	/**
	 * The key code that triggered this event.
	 * @see `EventType.KeyCodePress`
	 * @see `EventType.KeyCodeStatus`
	 * @see [List of key codes (external link)](http://api.kha.tech/kha/input/KeyCode.html)
	 */
	@:optional final keyCode: kha.input.KeyCode;
	/**
	 * The character representation of the key that triggered this event.
	 * @see `EventType.KeyCharPress`
	 * @see `StringUtil.canPrintChar()`
	 */
	@:optional final keyChar: String;
}

/**
 * Represents an event type.
 *
 * @see [Wiki: Events/Event Types](https://gitlab.com/koui/Koui/-/wikis/Documentation/Events#event-types)
 */
 enum abstract EventType(Int) {
	/**
	 * `MouseHover` events are fired if the mouse cursor is above an element.
	 *
	 * **Possible states:**
	 *
	 * - `Activated`: fired when the mouse first hovers the element
	 * - `Active`: (every frame when the mouse hovers the element)
	 * - `Deactivated`: (when the mouse is no longer on the element that was
	 *   hovered in the last frame). If an objects gets an `Deactivated` event,
	 *   another element might already receive an `Activated` event.
	 *
	 * **Further attributes:**
	 *
	 * - `mouseMoved`: might be `true` or `false` depending on whether the mouse
	 *   was moved or is stationary over the element.
	 */
	var MouseHover;

	/**
	 * `MouseClick` events are fired if any mouse button is clicked. Only
	 * hovered or focused elements can receive such events.
	 *
	 * **Possible states:**
	 *
	 * - `Activated`: Received by the hovered element if the mouse first clicks
	 *   on it
	 * - `Active`: Received by the focused element if any mouse button is
	 *   pressed
	 * - `Deactivated`: Received by the focused element when the mouse button
	 *   was released and the element is still hovered
	 * - `Cancelled`: The same as `Deactivated` but fired when the focused
	 *   element is no longer hovered
	 *
	 * **Further attributes:**
	 *
	 * - `mouseButton`: A `MouseButton` ([API]()) value describing the mouse
	 *   button that triggered this event.
	 */
	var MouseClick;

	/**
	 * A `MouseScroll` event is fired when the mouse wheel is rotated. Is it
	 *   sent to the currently hovered element.
	 *
	 * **Possible states:**
	 *
	 *  - `Active`: The only state of this event. Either the mouse is scrolled
	 *   or not.
	 *
	 * **Further attributes:**
	 *
	 * - `scrollDelta: Int`: The amount of mouse wheel rotation. The faster the
	 *   mouse is scrolled, the higher the value. It is positive if the mouse
	 *   wheel is scrolled down and negative if it is scrolled up.
	 */
	var MouseScroll;

	/**
	 * A `Focus` event is fired when the mouse clicks on a element that was not
	 * focused before. The previously focused element (might be `null` as well)
	 * is no longer focused then.
	 *
	 * **Possible states:**
	 *

	 * - `Activated`: Fired once when the element is focused
	 * - `Deactivated`: Fired once when the element is no longer focused. Also
	 *   fired when `EventHandler.clearFocus()` is called.
	 *
	 * **Further attributes:**
	 *
	 * - `mouseButton: MouseButton`: A `MouseButton` ([API]()) value describing
	 *   the mouse button that triggered this event. Focusing is still
	 *   button-independent!
	 */
	var Focus;

	/**
	 * A `KeyCharPress` event is fired on the first frame a key is pressed and
	 * after that on a certain time intervall to simulate a OS-like input
	 * behaviour. That intervall can be controlled by
	 * `EventHandler.KEY_REPEAT_DELAY` and `EventHandler.KEY_REPEAT_PERIOD`.
	 *
	 * Please note that this event is only fired by keys that represent a
	 * character due to internal limitations. For an event that also handles
	 * other keys, please refer to [`KeyCodePress`](#KeyCodePress).
	 *
	 * **Possible states:**
	 *
	 * - `Active`
	 *
	 * **Further attributes:**
	 *
	 * - `keyChar: String`: The character of the key that triggered the event.
	 *
	 * @see [`KeyCodePress`](#KeyCodePress)
	 */
	var KeyCharPress;

	/**
	 * A `KeyCodePress` event is fired on the first frame a key is pressed and
	 * after that on a certain time intervall to simulate a OS-like input
	 * behaviour. That intervall can be controlled by
	 * `EventHandler.KEY_REPEAT_DELAY` and `EventHandler.KEY_REPEAT_PERIOD`.
	 *
	 * **Possible states:**
	 *
	 * - `Activated`: Fired on the first frame a key is pressed down.
	 * - `Active`: Fired on all consecutive time steps of the intervall.
	 *
	 * **Further attributes:**
	 *
	 * - `keyCode: kha.input.KeyCode`: The key code that corresponds to the key
	 *   that triggered the event.
	 *
	 * @see [`KeyCharPress`](#KeyCharPress)
	 */
	var KeyCodePress;

	/**
	 * A `KeyCodeStatus` event is fired on the first frame a key was pressed down
	 * or was released.
	 *
	 * **Possible states:**
	 *
	 * - `Activated`: Fired on the first frame a key is pressed down.
	 * - `Deactivated`: Fired on the first frame after a key was released.
	 *
	 * **Further attributes:**
	 *
	 * - `keyCode: kha.input.KeyCode`: The key code that corresponds to the key
	 *   that triggered the event.
	 */
	var KeyCodeStatus;
}

/**
 * Represents the state of an event.
 *
 * @see [Wiki: Events/Event States](https://gitlab.com/koui/Koui/-/wikis/Documentation/Events#event-states)
 */
enum abstract EventState(Int) {
	var Activated;
	var Active;
	var Deactivated;
	var Cancelled;
}

/**
 * Represents a mouse button.
 */
enum abstract MouseButton(Int) from Int {
	var Left = 0;
	var Right = 1;
	var Middle = 2;
}
