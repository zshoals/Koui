package koui.effects;

import kha.Color;
import kha.graphics4.ConstantLocation;
import kha.graphics4.IndexBuffer;
import kha.graphics4.PipelineState;
import kha.graphics4.Usage;
import kha.graphics4.VertexBuffer;
import kha.graphics4.VertexData;
import kha.graphics4.VertexStructure;
import kha.Shaders;

import koui.elements.Element;

class BoxShadow extends Effect {
	#if (KOUI_EFFECTS_OFF || KOUI_EFFECTS_SHADOW_OFF)
	private static inline override function init() {}
	public static inline function draw(g2: kha.graphics2.Graphics, element: Element) {}

	#else

	// Use different draw positions for elements in certain layouts
	@:allow(koui.elements.Element)
	static var drawOffsetX = 0.0;
	@:allow(koui.elements.Element)
	static var drawOffsetY = 0.0;
	@:allow(koui.elements.Element)
	static var drawSizeX = -1;
	@:allow(koui.elements.Element)
	static var drawSizeY = -1;

	static var pipeline: PipelineState;
	static var indexBuffer: IndexBuffer;
	static var structure: VertexStructure;

	// Bind constants
	static var bBoxLeft: ConstantLocation;
	static var bBoxRight: ConstantLocation;
	static var bBoxTop: ConstantLocation;
	static var bBoxBottom: ConstantLocation;
	static var bColor: ConstantLocation;
	static var bRadius: ConstantLocation;
	static var bFalloff: ConstantLocation;

	public static override function init() {
		structure = new VertexStructure();
		structure.add("pos", VertexData.Float2);

		var indices = [0, 1, 2, 1, 2, 3];
		indexBuffer = new IndexBuffer(indices.length, Usage.StaticUsage);
		var ibuffer = indexBuffer.lock();
		for (i in 0...indices.length) {
			ibuffer[i] = indices[i];
		}
		indexBuffer.unlock();

		pipeline = new PipelineState();
		pipeline.inputLayout = [structure];
		pipeline.vertexShader = Shaders.shadow_vert;
		pipeline.fragmentShader = Shaders.shadow_frag;

		pipeline.alphaBlendSource = BlendOne;
		pipeline.alphaBlendDestination = BlendOne;
		pipeline.blendSource = SourceAlpha;
		pipeline.blendDestination = InverseSourceAlpha;
		pipeline.blendOperation = Add;

		pipeline.compile();

		bBoxLeft = pipeline.getConstantLocation("boxLeft");
		bBoxRight = pipeline.getConstantLocation("boxRight");
		bBoxTop = pipeline.getConstantLocation("boxTop");
		bBoxBottom = pipeline.getConstantLocation("boxBottom");
		bColor = pipeline.getConstantLocation("color");
		bRadius = pipeline.getConstantLocation("radius");
		bFalloff = pipeline.getConstantLocation("falloff");
	}

	@:access(koui.elements.Element)
	public static function draw(g2: kha.graphics2.Graphics, element: Element) {
		g2.end();
		var g = Koui.g4;
		g.begin();

		var propertyMap = koui.Theme.theme[element.tID];
		if (propertyMap["shadow_radius"] <= 0 || propertyMap["shadow_radius"] == null) {
			g.end();
			g2.begin(false);
			return;
		}

		var radius = propertyMap["shadow_radius"];
		var falloff: Float = propertyMap["shadow_falloff"];
		var colorValue: Int = propertyMap["shadow_color"];
		var color = Color.fromValue(colorValue);
		var colorR = color.R;
		var colorG = color.G;
		var colorB = color.B;
		var colorA = color.A;

		// Using canvas space instead of screen space for drawing if the shadow
		// is drawn onto a `kha.Canvas` object
		var screenW, screenH: Int;
		if (drawSizeX < 0) {
			screenW = kha.Window.get(0).width;
			screenH = kha.Window.get(0).height;
		} else {
			screenW = drawSizeX;
			screenH = drawSizeY;
		}

		// Calculate shadow quad positions in window space
		var posLeft = (element.drawX + drawOffsetX - radius) / screenW;
		var posRight = (element.drawX + drawOffsetX + element.drawWidth + radius) / screenW;
		var posTop = (screenH - (element.drawY + drawOffsetY - radius)) / screenH;
		var posBottom = (screenH - (element.drawY + drawOffsetY + element.drawHeight + radius)) / screenH;

		// 0 = screen center, (+/-)1 = border
		posLeft = posLeft * 2 - 1;
		posRight = posRight * 2 - 1;
		posTop = posTop * 2 - 1;
		posBottom = posBottom * 2 - 1;

		// Generate shadow quad mesh
		var vertices = [posLeft, posTop, posLeft, posBottom, posRight, posTop, posRight, posBottom];
		var vertexBuffer = new VertexBuffer(vertices.length, structure, Usage.StaticUsage);
		var buffer = vertexBuffer.lock();
		for (i in 0...Std.int(vertices.length / 2)) {
			buffer.set(i * 2 + 0, vertices[i * 2 + 0]);
			buffer.set(i * 2 + 1, vertices[i * 2 + 1]);
		}
		vertexBuffer.unlock();

		g.setPipeline(pipeline);
		g.setIndexBuffer(indexBuffer);
		g.setVertexBuffer(vertexBuffer);

		g.setFloat(bBoxLeft, element.drawX);
		g.setFloat(bBoxRight, element.drawX + element.drawWidth);
		#if (kha_opengl || kha_webgl)
		// OpenGL uses inverted y axis
		g.setFloat(bBoxTop, screenH - (element.drawY + drawOffsetX));
		g.setFloat(bBoxBottom, screenH - (element.drawY +  drawOffsetX + element.drawHeight));
		#else
		g.setFloat(bBoxTop, element.drawY + drawOffsetY + element.drawHeight);
		g.setFloat(bBoxBottom, element.drawY + drawOffsetY);
		#end

		g.setFloat4(bColor, colorR, colorG, colorB, colorA);
		g.setFloat(bRadius, radius);
		g.setFloat(bFalloff, falloff);

		g.drawIndexedVertices();

		g.end();
		g2.begin(false);
	}
	#end
}
