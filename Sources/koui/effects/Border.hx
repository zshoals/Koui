package koui.effects;

import koui.elements.Element;

class Border extends Effect {
	#if (KOUI_EFFECTS_OFF || KOUI_EFFECTS_BORDER_OFF)
	public static inline function draw(g: kha.graphics2.Graphics, element: Element) {}

	#else
	@:access(koui.elements.Element)
	public static function draw(g: kha.graphics2.Graphics, element: Element) {
		var propertyMap = koui.Theme.theme[element.tID];
		if (propertyMap["border_size"] == 0 || propertyMap["border_size"] == null) return;

		var thickness = propertyMap["border_size"];
		g.color = propertyMap["border_color"];

		switch(propertyMap["border_style"]) {
			case "inset":
				g.drawRect(element.drawX + thickness/2, element.drawY + thickness/2,
					element.drawWidth - thickness, element.drawHeight - thickness, thickness);
			case "middle":
				g.drawRect(element.drawX, element.drawY, element.drawWidth, element.drawHeight, thickness);
			case "outset":
				g.drawRect(element.drawX - thickness/2, element.drawY - thickness/2,
					element.drawWidth + thickness, element.drawHeight + thickness, thickness);
			default:
				trace('Koui warning: Border style ${propertyMap["border_style"]} not supported!');
		}
	}
	#end
}
