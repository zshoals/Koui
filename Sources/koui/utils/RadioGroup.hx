package koui.utils;

import koui.elements.RadioButton;


class RadioGroup {
	public var activeButton(default, null): Null<RadioButton>;

	var buttons: Array<RadioButton> = new Array();

	public function new() {}

	public function add(button: RadioButton) {
		buttons.push(button);

		if (buttons.length == 1) setActiveButton(button);
	}

	public function setActiveButton(button: RadioButton) {
		activeButton = button;

		for (bt in buttons) {
			bt.isActive = false;
		}

		button.isActive = true;
	}
}