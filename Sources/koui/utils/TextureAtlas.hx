package koui.utils;

import kha.graphics2.Graphics;

import koui.elements.Element;

class TextureAtlas {
	/**
	 * Draws a part of the background texture of the given element to the given
	 *`Graphics` object.

	 * @param g
	 * @param element
	 * @param divisionsX How many times the texture is divided (tiled) on the x axis (default: 1)
	 * @param divisionsY How many times the texture is divided (tiled) on the y axis (default: 1)
	 * @param offsetX The x axis index of the tile that should be drawn, see `divisionsX` (default: 0)
	 * @param offsetY The y axis index of the tile that should be drawn, see `divisionsY` (default: 0)
	 * @param drawX Custom drawing position
	 * @param drawX Custom drawing position
	 * @return `true` if the background texture exists and was drawn, else `false`
	 */
	@:access(koui.elements.Element)
	public static function drawFromAtlas(g: Graphics, element: Element, divisionsX: Int = 1, divisionsY: Int = 1,
			offsetX: Int = 0, offsetY: Int = 0, ?drawX: Int, ?drawY: Int): Bool {

		var texture = kha.Assets.images.get(element.p("texture"));

		// Draw element without textures
		if (texture == null) {
			return false;
		}

		if (drawX == null) drawX = element.drawX;
		if (drawY == null) drawY = element.drawY;

		var atlasX = 0;
		var atlasY = 0;
		var atlasW: Int;
		var atlasH: Int;

		if (element.p("atlas")) {
			atlasX = element.p("atlas_x");
			atlasY = element.p("atlas_y");
			atlasW = element.p("atlas_width");
			atlasH = element.p("atlas_height");
		} else {
			atlasW = Std.int(texture.width / divisionsX);
			atlasH = Std.int(texture.height / divisionsY);
		}

		atlasX += atlasW * offsetX;
		atlasY += atlasH * offsetY;

		g.color = 0xffffffff;
		if (element.p("texture_scale")) {
			g.drawScaledSubImage(texture, drawX, drawY, element.drawWidth, element.drawHeight, atlasX, atlasY, atlasW, atlasH);
		} else {
			g.drawSubImage(texture, drawX, drawY, atlasX, atlasY, atlasW, atlasH);
		}

		return true;
	}
}
