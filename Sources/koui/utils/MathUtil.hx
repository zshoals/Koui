package koui.utils;

@:pure
class MathUtil {
	public static function hitbox(pointX: Int, pointY: Int, posX: Float, posY: Float, sizeX: Float, sizeY: Float) {
		return pointX >= posX && pointX <= posX + sizeX && pointY >= posY && pointY <= posY + sizeY;
	}

	public static function mapToRange(value: Float, fromLeft: Float, fromRight: Float, toLeft: Float, toRight: Float): Float {
		return (value - fromLeft) * (toRight - toLeft) / (fromRight - fromLeft) + toLeft;
	}

	public static inline function clamp(value: Float, minValue: Float, maxValue: Float): Float {
		return Math.max(minValue, Math.min(maxValue, value));
	}

	public static inline function clampI(value: Int, minValue: Int, maxValue: Int): Int {
		return Std.int(Math.max(minValue, Math.min(maxValue, value)));
	}

	/**
	 * Rounds a value to the given precision.
	 *
	 * @param value The value that should get rounded
	 * @param precision Number of decimal places
	 */
	public static function roundPrecision(value: Float, precision = 0):Float {
		value *= Math.pow(10, precision);
		value = Std.int(value);
		value /= Math.pow(10, precision);
		return value;
	}

	public static function arraySum(array: Array<Float>): Float {
		var res = 0.0;
		for (elem in array) res += elem;
		return res;
	}
}
