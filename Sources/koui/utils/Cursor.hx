package koui.utils;

/**
 * This class handles all cursor-related tasks.
 */
@:allow(koui.events.EventHandler)
@:allow(koui.Koui)
class Cursor {
	static var mouse: kha.input.Mouse;
	static var mouseX = 0;
	static var mouseY = 0;

	static var type = CursorType.Default;
	static var systemMouseHidden = false;

	static function init() {
		mouse = kha.input.Mouse.get();
	}

	/**
	 * Set the mouse position.
	 *
	 * @param x The x position
	 * @param y The y position
	 */
	public static function setPosition(x: Int, y: Int) {
		mouseX = x;
		mouseY = y;
	}

	/**
	 * Set the type of the mouse cursor.
	 *
	 * @param cursorType The cursor type
	 */
	public static function setCursor(cursorType: CursorType) {
		type = cursorType;

		switch (type) {
			case Default:
				mouse.showSystemCursor();
				systemMouseHidden = false;
			default:
				mouse.hideSystemCursor();
				systemMouseHidden = true;
		}
	}

	static function getCursorEnumValue(name: String): CursorType {
		if (name == null) return Default;

		return switch (name.toLowerCase()) {
			case "default": Default;
			case "pointer": Pointer;
			case "text": Text;
			case "wait": Wait;
			case "notallowed": NotAllowed;
			default: Default;
		}
	}

	#if KOUI_MOUSE_ACCEL
	static function calculateAccelMousePosition(x: Int, y: Int, deltaX: Int, deltaY: Int) {
		mouseX = x;
		mouseX += Std.int(deltaX * 0.7);
		mouseY = y;
		mouseY += Std.int(deltaY * 0.7);

		if (deltaX == 0) mouseX += Std.int((x - mouseX) * 0.3);
		if (deltaY == 0) mouseY += Std.int((y - mouseY) * 0.3);
	}
	#end

	/**
	 * Draws the cursor onto the screen.
	 *
	 * @param g `The kha.graphics2.Graphics` used to draw the cursor
	 */
	static function draw(g: kha.graphics2.Graphics) {
		g.color = 0xffffffff;
		switch (type) {
			case Default:
			case Text:
				g.drawScaledSubImage(kha.Assets.images.cursor_text, 0, 0, 64, 64, mouseX - 16, mouseY - 16, 32, 32);
			case NotAllowed:
				g.drawScaledSubImage(kha.Assets.images.cursor_notallowed, 0, 0, 64, 64, mouseX - 16, mouseY - 16, 32, 32);
			default:
		}
	}
}

/**
 * Represents a mouse cursor type. The names of the individual values are
 * inspired by the names used in CSS.
 *
 * @see [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/CSS/cursor)
 */
enum abstract CursorType(Int) from Int to Int {
	/**
	 * The default cursor.
	 */
	var Default;
	/**
	 * A pointer cursor, often used to indicate an action that can be triggered
	 * with a mouse click.
	 */
	var Pointer;
	/**
	 * A text cursor, used in `TextInput` for example.
	 */
	var Text;
	/**
	 * A cursor indicating that some process is going on in the background.
	 */
	var Wait;
	/**
	 * A cursor indicating that the action that could be triggered is not
	 * allowed. It is used for disabled elements for example.
	 */
	var NotAllowed;
}
