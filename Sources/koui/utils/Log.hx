package koui.utils;

/**
 * Logging utility.
 */
class Log {
    /**
     * Returns an error message in the format `"[Koui Error] <message>"`.
     *
     * @param message The message
     */
    public static inline function error(message: String) {
        throw "[Koui Error] " + message;
    }
}