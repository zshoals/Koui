package koui.elements;

import koui.effects.Border;
import koui.events.Events;
import koui.utils.TextureAtlas;
import koui.utils.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A `Checkbox` is a button that represents an option, it can either be switched
 * on or off. It has a text label that is describing the function of the
 * checkbox.
 *
 * ```haxe
 * // Construct a new Checkbox with the label "Option"
 * var myCheckbox = new Checkbox("Option");
 * // Make the checkbox checked (select it)
 * myCheckbox.isActive = true;
 * ```
 */
class Checkbox extends Element {
	/**
	 * The label of this checkbox.
	 */
	public var label = "";
	/**
	 * `true` when this checkbox is active, otherwise `false`.
	 */
	public var isActive = false;

	var innerMargin: Int = 0;

	var isHovered = false;
	var isClicked = false;

	/**
	 * Creates a new `Checkbox`.
	 * @param label The label of this checkbox.
	 */
	public function new(label: String) {
		super();
		this.label = label;
	}

	override function onTIDChange() {
		width = p("width");
		height = p("height");

		innerMargin = Std.int((height - p("check_size")) / 2);
	}

	#if !KOUI_EVENTS_OFF
	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated: isHovered = true;
			case Active:
			case Deactivated, Cancelled: isHovered = false;
		}
	}

	override function _onClick(event: Event) {
		if (event.mouseButton != Left) return;

		switch (event.state) {
			case Activated: isClicked = true;
			case Active:
			case Deactivated:
				isClicked = false;
				isActive = !isActive;
			case Cancelled:
				isClicked = false;
		}
	}
	#end

	public override function draw(g: kha.graphics2.Graphics) {
		var check_size: Int = p("check_size");

		if (p("texture") != null) {
			innerMargin = Std.int((drawHeight - p("atlas_height")) / 2);
		}

		var atlasOffsetX = isActive ? 1 : 0;
		var atlasOffsetY = isClicked ? 2 : (isHovered ? 1 : 0);

		if (!TextureAtlas.drawFromAtlas(g, this, 2, 3, atlasOffsetX, atlasOffsetY, drawX + innerMargin, drawY + innerMargin)) {
			g.color = p("color_bg");

			if (isActive) {
				if (isClicked) g.color = p("color_sec_clicked");
				else if (isHovered) g.color = p("color_sec_hover");
				else g.color = p("color_sec");
			} else {
				if (isClicked) g.color = p("color_clicked");
				else if (isHovered) g.color = p("color_hover");
			}

			g.fillRect(drawX + innerMargin, drawY + (drawHeight - check_size) / 2, check_size, check_size);
		}

		g.color = p("color_text");
		g.font = ThemeUtil.getFont();
		g.fontSize = p("font_size");

		var offsetLeft = innerMargin * 2 + check_size;
		g.drawAlignedString(label, drawX + offsetLeft, drawY + drawHeight / 2, TextLeft, TextMiddle);

		Border.draw(g, this);
	}
}
