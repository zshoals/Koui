package koui.elements;

import haxe.ds.Vector;

import koui.Koui;
import koui.Theme;
import koui.elements.layouts.ICoordinateSystem;
import koui.elements.layouts.Layout;
import koui.elements.layouts.ScrollPane;
import koui.events.EventHandler;
import koui.events.Events;
import koui.utils.Log;
import koui.utils.MathUtil;


/**
 * The base class of all elements. **Do not use this class directly**!
 */
@:allow(koui.Koui)
@:allow(koui.events.EventHandler)
@:autoBuild(koui.utils.ThemeUtil.buildSubElement())
class Element {
	/**
	 * The x position of this element.
	 */
	public var posX(default, set): Int;
	/**
	 * The y position of this element.
	 */
	public var posY(default, set): Int;
	/**
	 * The width of this element.
	 */
	public var width(default, set): Int;
	/**
	 * The height of this element.
	 */
	public var height(default, set): Int;

	/**
	 * The anchor of this element. Used for the layout.
	 * @see `Anchor`
	 */
	public var anchor = Anchor.TopLeft;

	/**
	 * `true` if the element is disabled and should not react to events. Also,
	 * elements might look different if disabled.
	 */
	public var disabled = false;

	/**
	 * If `false`, the element is not visible and will not react to any events.
	 */
	public var visible = true;

	/**
	 * The theme ID of this object. Use this to select which theme settings to
	 * apply.
	 */
	public var tID(default, set) = "_root";

	/**
	 * The x position of this element, used for drawing and event handling.
	 */
	var drawX(default, null): Int;
	/**
	  * The y position of this element, used for drawing and event handling.
	  */
	var drawY(default, null): Int;
	/**
	  * The width of this element, used for drawing and event handling.
	  */
	var drawWidth(default, null): Int;
	/**
	  * The height of this element, used for drawing and event handling.
	  */
	var drawHeight(default, null): Int;
	/**
	 * The x position of this element, used for the layout.
	 */
	var layoutX(default, set): Int;
	/**
	  * The y position of this element, used for the layout.
	  */
	var layoutY(default, set): Int;
	/**
	  * The width of this element, used for the layout.
	  */
	var layoutWidth(default, set): Int;
	/**
	  * The height of this element, used for the layout.
	  */
	var layoutHeight(default, set): Int;

	/**
	 * The layout this element belongs to.
	 */
	@:allow(koui.elements.layouts.Layout)
	var layout: Layout = null;

	var onMouseHoverFunc: Null<Event -> Void>;
	var onMouseClickFunc: Null<Event -> Void>;
	var onMouseScrollFunc: Null<Event -> Void>;
	var onFocusFunc: Null<Event -> Void>;
	var onKeyCharPressFunc: Null<Event -> Void>;
	var onKeyCodePressFunc: Null<Event -> Void>;
	var onKeyCodeStatusFunc: Null<Event -> Void>;

	function new(posX: Int = 0, posY: Int = 0, width: Int = 0, height: Int = 0) {
		this.posX = this.layoutX = posX;
		this.posY = this.layoutY = posY;
		this.width = this.layoutWidth = width;
		this.height = this.layoutHeight = height;

		EventHandler.registerElement(this);
	}

	function set_posX(value: Int) { return posX = layoutX = value; }
	function set_posY(value: Int) { return posY = layoutY = value; }
	function set_width(value: Int) { return width = layoutWidth = value; }
	function set_height(value: Int) { return height = layoutHeight = value; }
	function set_layoutX(value: Int) { return layoutX = drawX = value; }
	function set_layoutY(value: Int) { return layoutY = drawY = value; }
	function set_layoutWidth(value: Int) { return layoutWidth = drawWidth = value; }
	function set_layoutHeight(value: Int) { return layoutHeight = drawHeight = value; }

	function set_tID(value: String): String {
		if (tID != value) {
			tID = value;
			this.onTIDChange();
		}
		return tID;
	}

	/**
	 * Sets the position of this element. You can also change the position
	 * per axis, see [`posX`](#posX) and [`posY`](#posY).
	 */
	public inline function setPosition(posX: Int, posY: Int) {
		this.posX = posX;
		this.posY = posY;
	}

	/**
	 * Sets the size of this element. You can also change the size for each
	 * individual side, see [`width`](#width) and [`height`](#height).
	 */
	public inline function setSize(width: Int, height: Int) {
		this.width = width;
		this.height = height;
	}

	/**
	 * Return the offset of the element taking only layouts with different
	 * drawing transformations (like ScrollPanes) into account.
	 *
	 * **Warning**: Don't use this method too often, it can be costly when used
	 * too much.
	 */
	public function getLayoutOffset(): Vector<Int> {
		var offset = new Vector<Int>(2);
		offset[0] = 0;
		offset[1] = 0;

		// Start with `this` to be less redundant with the following code
		var currentParent = this;
		while(currentParent.layout != null) {
			currentParent = currentParent.layout;

			if (Std.is(currentParent, ICoordinateSystem)) {
				var par = cast(currentParent, Layout);
				offset[0] -= par.layoutX;
				offset[1] -= par.layoutY;

				if (Std.is(par, ScrollPane)) {
					var sp = cast(par, ScrollPane);
					offset[0] += Std.int(@:privateAccess sp.scrollX);
					offset[1] += Std.int(@:privateAccess sp.scrollY);
				}
			}
		}

		return offset;
	}

	/**
	 * Returns the value of the given property based on this element's `tID`.
	 *
	 * The name of this function is purposely kept short to reduce writing time
	 * for multiple calls.
	 *
	 * **Warning:** Using nonexistant properties will result in undefined
	 * behaviour. For example, g.color might default to full transparency when
	 * a property doesn't exist. When something does not work as expected, check
	 * the property name for spelling issues first!
	 *
	 * @param property The name of the theme property from which to get the value
	 */
	public final inline function p(property: String): Null<Dynamic> {
		return Theme.theme[this.tID][property];
	}

	/**
	 * Returns `true` if this element is at the given position. Used internally
	 * for event handling most of the time. Elements may override this method
	 * to provide more detailed mouse interaction.
	 * If the element is invisible, `false` is returned.
	 */
	public inline function isAtPosition(x: Int, y: Int) {
		// Use drawing measures to take overlays into account
		if (!visible) return false;
		return MathUtil.hitbox(x, y, this.drawX, this.drawY, this.drawWidth, this.drawHeight);
	}

	/**
	 * Call the given function when a `MouseHover` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onMouseHover(func: Event -> Void) {
		this.onMouseHoverFunc = func;
	}

	/**
	 * Call the given function when a `MouseClick` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onMouseClick(func: Event -> Void) {
		this.onMouseClickFunc = func;
	}

	/**
	 * Call the given function when a `MouseScroll` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onMouseScroll(func: Event -> Void) {
		this.onMouseScrollFunc = func;
	}

	/**
	 * Call the given function when a `Focus` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onFocus(func: Event -> Void) {
		this.onFocusFunc = func;
	}

	/**
	 * Call the given function when a `KeyPress` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onKeyCharPress(func: Event -> Void) {
		this.onKeyCharPressFunc = func;
	}

	/**
	 * Call the given function when a `KeyTyped` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onKeyCodePress(func: Event -> Void) {
		this.onKeyCodePressFunc = func;
	}

	/**
	 * Call the given function when a `KeyCode` event was received by this element.
	 * @param func The callback function
	 */
	public final inline function onKeyCodeStatus(func: Event -> Void) {
		this.onKeyCodeStatusFunc = func;
	}

	/**
	 * Returns a string representation of this element:
	 * `"Element: <[ClassName]>"`
	 */
	public inline function toString(): String {
		return "Element: <" + Type.getClassName(Type.getClass(this)) + ">";
	}

	function draw(g: kha.graphics2.Graphics) {
		throw Log.error("draw() function must be overriden by element!");
	}

	function drawOverlay(g: kha.graphics2.Graphics) {}

	// Internal use only
	function _onHover(event: Event) {}
	function _onClick(event: Event) {}
	function _onScroll(event: Event) {}
	function _onKeyCharPress(event: Event) {}
	function _onKeyCodePress(event: Event) {}
	function _onKeyCodeStatus(event: Event) {}
	function _onFocus(event: Event) {}

	function onTIDChange() {}
}
