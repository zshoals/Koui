package koui.elements;

import kha.graphics2.HorTextAlignment;
import kha.graphics2.VerTextAlignment;

import koui.effects.Border;
import koui.utils.TextureAtlas;
import koui.utils.ThemeUtil;

using kha.graphics2.GraphicsExtension;

class Label extends Element {
	public var text = "";
	public var alignmentHor = HorTextAlignment.TextLeft;
	public var alignmentVert = VerTextAlignment.TextTop;

	var shadowColor: kha.Color;
	var shadowX: Null<Int> = 0;
	var shadowY: Null<Int> = 0;

	/**
	 * Creates a new `Label` with the given text.
	 *
	 * @param text The text to display
	 */
	public function new(text: String) {
		super();
		this.text = text;
	}

	override function onTIDChange() {
		shadowColor = p("shadow_color");
		shadowX = p("shadow_x");
		shadowY = p("shadow_y");
		if (shadowX == null) shadowX = 0;
		if (shadowY == null) shadowY = 0;

		if (p("texture") == null) {
			width = p("width");
			height = p("height");
		} else {
			if (p("atlas")) {
				width = p("atlas_width");
				height = p("atlas_height");
			} else {
				width = kha.Assets.images.get(p("texture")).width;
				height = kha.Assets.images.get(p("texture")).height;
			}
		}
	}

	public override function draw(g: kha.graphics2.Graphics) {
		TextureAtlas.drawFromAtlas(g, this, 1, 1, 0, 0);

		var margin: Int = p("margin");
		var drawAtX = drawX;
		var drawAtY = drawY;

		switch (this.anchor) {
			case TopLeft:
				drawAtX += margin;
			case TopCenter:
				drawAtX = Std.int(drawX + drawWidth / 2 + margin);
			case TopRight:
				drawAtX = drawX + drawWidth - margin;
			case MiddleLeft:
				drawAtX += margin;
				drawAtY = Std.int(drawY + drawHeight / 2);
			case MiddleCenter:
				drawAtX = Std.int(drawX + drawWidth / 2 + margin);
				drawAtY = Std.int(drawY + drawHeight / 2);
			case MiddleRight:
				drawAtX = drawX + drawWidth - margin;
				drawAtY = Std.int(drawY + drawHeight / 2);
			case BottomLeft:
				drawAtX += margin;
				drawAtY = drawY + drawHeight;
			case BottomCenter:
				drawAtX = Std.int(drawX + drawWidth / 2 + margin);
				drawAtY = drawY + drawHeight;
			case BottomRight:
				drawAtX = drawX + drawWidth - margin;
				drawAtY = drawY + drawHeight;
		}

		g.pipeline = Koui.textPipeline;

		g.fontSize = p("font_size");
		g.font = ThemeUtil.getFont();

		// Draw shadow if specified
		if (shadowX != 0 && shadowY != 0) {
			g.color = shadowColor;
			g.drawAlignedString(text, drawAtX + shadowX, drawAtY + shadowY, alignmentHor, alignmentVert);
		}

		g.color = p("color_text");
		g.drawAlignedString(text, drawAtX, drawAtY, alignmentHor, alignmentVert);
		g.pipeline = null;

		Border.draw(g, this);
	}
}
