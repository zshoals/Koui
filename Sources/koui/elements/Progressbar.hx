package koui.elements;

import kha.FastFloat;
import kha.graphics2.Graphics;

import koui.effects.Border;
import koui.utils.Log;
import koui.utils.MathUtil;
import koui.utils.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * Displays a progress bar.
 */
class Progressbar extends Element {
	/**
	 * The current value (in the intervall `[minValue, maxValue]`).
	 */
	public var value(default, set): FastFloat = 0.0;

	/**
	 * The current progress (in the intervall `[0, 1]`).
	 */
	public var progress(default, null): FastFloat = 0.0;

	/**
	 * The label of this progress bar.
	 * Tip: Use Haxe's [template system](https://haxe.org/manual/std-template.html)
	 * with [`::value::`](#value), [`::progress::`](#progress),
	 * [`::minValue::`](#minValue) or [`::maxValue::`](#maxValue)! Other
	 * template variables are not supported.
	 * ```haxe
	 * myProgressbar.label = "Progress: ::progress::%";
	 * ```
	 *
	 * @see [`precision`](#precision)
	 */
	public var label = "";

	/**
	 * The amount of decimal places of the value of this progress bar. Used only
	 * for displaying the label. The value itself is not changed by this
	 * variable. If you need to change the value to this precision, use
	 * `MathUtil.roundPrecision()`.
	 *
	 * @see `MathUtil.roundPrecision()`
	 */
	public var precision = 0;

	/**
	 * The smallest possible value of this progress bar.
	 */
	public var minValue(default, set): FastFloat = 0.0;
	/**
	 * The largest possible value of this progress bar.
	 */
	public var maxValue(default, set): FastFloat = 1.0;


	var marginX = 0;
	var marginY = 0;
	var orientation = "right";

	/**
	 * Creates a new `Progressbar`.
	 * @param minValue The minimum value (default: `0.0`)
	 * @param maxValue The maximum value (default: `1.0`)
	 */
	public function new(minValue: FastFloat = 0.0, maxValue: FastFloat = 1.0) {
		super();

		if (maxValue < minValue)
			throw Log.error("Progressbar: maxValue must be larger than minValue!");

		this.minValue = this.value = minValue;
		this.maxValue = maxValue;
	}

	@:dox(hide)
	public function set_value(value: FastFloat) {
		progress = MathUtil.mapToRange(value, minValue, maxValue, 0, 1);
		return this.value = value;
	}

	@:dox(hide)
	public function set_minValue(minValue: FastFloat) {
		if (minValue > maxValue)
			throw Log.error("Progressbar: minValue must be smaller than maxValue!");

		return this.minValue = minValue;
	}

	@:dox(hide)
	public function set_maxValue(maxValue: FastFloat) {
		if (maxValue < minValue)
			throw Log.error("Progressbar: maxValue must be larger than minValue!");

		return this.maxValue = maxValue;
	}

	override function onTIDChange() {
		width = p("width");
		height = p("height");

		marginX = p("margin_x");
		marginY = p("margin_y");
		orientation = p("orientation");
	}

	public override function draw(g: Graphics) {
		g.color = p("color_bg");
		g.fillRect(drawX, drawY, drawWidth, drawHeight);

		g.color = p("color_sec");

		switch (orientation) {
			case "right":
				var progressWidth: FastFloat = (drawWidth - marginX * 2) * progress;
				g.fillRect(drawX + marginX, drawY + marginY, progressWidth, drawHeight - marginY * 2);

			case "left":
				var progressWidth: FastFloat = (drawWidth - marginX * 2) * progress;
				g.fillRect(drawX + marginX + drawWidth - progressWidth, drawY + marginY, progressWidth, drawHeight - marginY * 2);

			case "bottom":
				var heightProgress: FastFloat = (drawHeight - marginY * 2) * progress;
				g.fillRect(drawX + marginX, drawY + marginY, drawWidth - marginX * 2, heightProgress);

			case "up":
				var heightProgress: FastFloat = (drawHeight - marginY * 2) * progress;
				g.fillRect(drawX + marginX, drawY + marginY + drawHeight - heightProgress,  drawWidth - marginX * 2, heightProgress);
		}

		var drawnLabel = new haxe.Template(label).execute(
			{progress: progress, value: MathUtil.roundPrecision(value, precision),
			 minValue: minValue, maxValue: maxValue});

		g.font = ThemeUtil.getFont();
		g.fontSize = p("font_size");
		g.color = p("color_text");
		g.drawAlignedString(drawnLabel, drawX + drawWidth / 2, drawY + drawHeight / 2, TextCenter, TextMiddle);

		Border.draw(g, this);
	}
}
