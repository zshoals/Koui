package koui.elements;

import kha.FastFloat;
import kha.graphics2.VerTextAlignment;

import koui.effects.Border;
import koui.events.EventHandler;
import koui.events.Events;
import koui.utils.MathUtil;
import koui.utils.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A slider is an element for controlling a value inside a certain range.
 * Sliders also have an attribute called [`precision`](#precision) that defines
 * the number of decimal places the value can have.
 *
 * ![Slider screenshot](https://gitlab.com/koui/Koui/-/wikis/images/elements/element_slider.png)
 *
 * ```haxe
 * // Construct a new Slider with values between 0.0 and 1.0
 * var mySlider = new Slider(0.0, 1.0);
 *
 * // Only accept whole numbers
 * mySlider.precision = 0;
 *
 * // Accept two decimal places
 * mySlider.precision = 2:
 * ```
 *
 * @see [Wiki: Slider](https://gitlab.com/koui/Koui/-/wikis/Documentation/Elements/Slider)
 */
class Slider extends Element {
	/**
	 * The current value of this element.
	 */
	public var value: FastFloat = 0.0;
	/**
	 * The number of decimal places the value of this element has.
	 */
	public var precision = 0;

	var minValue: FastFloat = 0.0;
	var maxValue: FastFloat = 1.0;

	var isHovered = false;
	var isClicked = false;
	var clickOffset = 0;

	var buttonWidth = 0;

	/**
	 * Creates a new `Slider` with the given range.
	 *
	 * @param minValue The lower bound of the slider's range
	 * @param maxValue The upper bound of the slider's range
	 */
	public function new(minValue: FastFloat, maxValue: FastFloat) {
		super();

		this.minValue = minValue;
		this.maxValue = maxValue;

		if (maxValue - minValue < 10) precision = 1;
	}

	override function onTIDChange() {
		if (p("texture") == null) {
			width = p("width");
			height = p("height");
		}
		else {
			width = p("atlas_width");
			height = p("atlas_height");
		}

		if (p("texture_button") == null) {
			buttonWidth = p("button_width");
		} else {
			buttonWidth = p("atlas_button_width");
		}
	}

	#if !KOUI_EVENTS_OFF
	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated: isHovered = true;
			case Active:
			case Deactivated, Cancelled: isHovered = false;
		}
	}

	override function _onClick(event: Event) {
		if (event.mouseButton != Left) return;

		switch (event.state) {
			case Activated:
				isClicked = true;
				clickOffset = this.getLayoutOffset()[0];
				EventHandler.block(this);
			case Active:
				this.value = MathUtil.mapToRange(
					EventHandler.mouseX + clickOffset,
					drawX + buttonWidth / 2, drawX + drawWidth - buttonWidth / 2,
					minValue, maxValue);
				this.value = MathUtil.clamp(value, minValue, maxValue);
			case Deactivated, Cancelled:
				isClicked = false;
				EventHandler.unblock();
		}
	}
	#end

	override function draw(g: kha.graphics2.Graphics) {
		var imageName: String = p("texture");
		if (imageName == null) {
			g.color = p("color_bg");
			g.fillRect(drawX, drawY, drawWidth, drawHeight);
		}

		else {
			var atlasX: Int = p("atlas_x");
			var atlasY: Int = p("atlas_y");
			var atlasW: Int = p("atlas_width");
			var atlasH: Int = p("atlas_height");

			width = atlasW;
			height = atlasH;
			drawWidth = atlasW;
			drawHeight = atlasH;

			g.color = 0xffffffff;
			if (p("texture_scale")) {
				g.drawScaledSubImage(kha.Assets.images.get(imageName), drawX, drawY, drawWidth, drawHeight, atlasX, atlasY, atlasW, atlasH);
			} else {
				g.drawSubImage(kha.Assets.images.get(imageName), drawX, drawY, atlasX, atlasY, atlasW, atlasH);
			}
		}

		var sliderButtonImgName = p("texture_button");
		if (sliderButtonImgName == null) {
			g.color = p("color_hover");
			if (isClicked) g.color = p("color_sec");
			else if (isHovered) g.color = p("color_clicked");

			g.fillRect(calculateSliderPosition(), drawY, buttonWidth, drawHeight);
		}

		else {
			var atlasX: Int = p("atlas_button_x");
			var atlasY: Int = p("atlas_button_y");
			var atlasW: Int = p("atlas_button_width");
			var atlasH: Int = p("atlas_button_height");

			if (isClicked) atlasY += atlasH * 2;
			else if (isHovered) atlasY += atlasH;

			g.color = 0xffffffff;
			if (p("texture_button_scale")) {
				g.drawScaledSubImage(kha.Assets.images.get(sliderButtonImgName), calculateSliderPosition(), drawY, drawWidth, drawHeight, atlasX, atlasY, atlasW, atlasH);
			} else {
				g.drawSubImage(kha.Assets.images.get(sliderButtonImgName), calculateSliderPosition(), drawY, atlasX, atlasY, atlasW, atlasH);
			}
		}

		g.color = p("color_text");
		g.font = ThemeUtil.getFont();
		g.fontSize = p("font_size");

		var pTextPosition: String = p("text_position");
		var textPosY = drawY + drawHeight;
		var textAlignY: VerTextAlignment = TextTop;
		switch (pTextPosition) {
			case "top":
				textPosY = drawY;
				textAlignY = TextBottom;
			case "middle":
				textPosY = Std.int(drawY + drawHeight / 2);
				textAlignY = TextMiddle;
			default:
		}

		if (p("show_range")) {
			g.drawAlignedString("" + minValue, drawX, textPosY, TextLeft, textAlignY);
			g.drawAlignedString("" + maxValue, drawX + drawWidth, textPosY, TextRight, textAlignY);
		}

		if (p("show_value")) {
			g.drawAlignedString("" + MathUtil.roundPrecision(value, this.precision), drawX + drawWidth / 2, textPosY, TextCenter, textAlignY);
		}

		Border.draw(g, this);
	}

	inline function calculateSliderPosition(): Float {
		return MathUtil.mapToRange(value, minValue, maxValue, drawX, drawX + drawWidth - buttonWidth);
	}
}
