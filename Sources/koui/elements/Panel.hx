package koui.elements;

import koui.effects.Border;
import koui.effects.BoxShadow;

class Panel extends Element {
	public function new() {
		super();
	}

	public override function draw(g: kha.graphics2.Graphics) {
		BoxShadow.draw(g, this);

		g.color = p("color_bg");
		g.fillRect(drawX, drawY, drawWidth, drawHeight);

		Border.draw(g, this);
	}
}
