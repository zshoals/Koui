package koui.elements;

import koui.effects.Border;
import koui.effects.BoxShadow;
import koui.events.Events;
import koui.utils.TextureAtlas;
import koui.utils.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A button is an element that performs an action when clicked. It has a text
 * label that is describing the function of the button.
 *
 * ```haxe
 * // Construct a new Button with the label "Quit"
 * var myButton = new Button("Quit");
 *
 * myButton.onClick(function(event: Event) {
 *     // If the left mouse button is released over the button, quit the application
 *     if (event.mouseButton == Left && event.state == Deactivated) {
 *          kha.System.stop();
 *     }
 * });
 * ```
 */
class Button extends Element {
	/**
	 * The label of the button.
	 */
	public var label = "";

	var isHovered = false;
	var isClicked = false;

	/**
	 * Creates a new `Button` with the label defined in `label`.
	 *
	 * @param label The label of this button.
	 */
	public function new(label: String) {
		super();

		this.label = label;
	}

	#if !KOUI_EVENTS_OFF
	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated: isHovered = true;
			case Active:
			case Deactivated, Cancelled: isHovered = false;
		}
	}

	override function _onClick(event: Event) {
		if (event.mouseButton != Left) return;

		switch (event.state) {
			case Activated: isClicked = true;
			case Active:
			case Deactivated, Cancelled: isClicked = false;
		}
	}
	#end

	override function onTIDChange() {
		if (p("texture") == null) {
			width = p("width");
			height = p("height");
		} else {
			if (p("atlas")) {
				width = p("atlas_width");
				height = p("atlas_height");
			} else {
				width = kha.Assets.images.get(p("texture")).width;
				height = Std.int(kha.Assets.images.get(p("texture")).height / 3);
			}
		}
	}

	public override function draw(g: kha.graphics2.Graphics) {
		BoxShadow.draw(g, this);

		var atlasOffsetY = isClicked ? 2 : (isHovered ? 1 : 0);

		if (!TextureAtlas.drawFromAtlas(g, this, 1, 3, 0, atlasOffsetY)) {
			g.color = p("color_bg");
			if (isClicked) g.color = p("color_clicked");
			else if (isHovered) g.color = p("color_hover");

			g.fillRect(drawX, drawY, drawWidth, drawHeight);
		}

		g.font = ThemeUtil.getFont();
		g.fontSize = p("font_size");
		g.color = p("color_text");
		g.drawAlignedString(label, drawX + drawWidth / 2, drawY + drawHeight / 2, TextCenter, TextMiddle);

		Border.draw(g, this);
	}
}
