package koui.elements;

import koui.effects.Border;
import koui.events.Events;
import koui.utils.RadioGroup;
import koui.utils.TextureAtlas;
import koui.utils.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A radio button, used to select exactly one option from a set of options.
 * @see `RadioGroup`
 */
class RadioButton extends Element {
	/**
	 * The label of this radio button.
	 */
	public var label = "";
	/**
	 * `true` when this radio button is active, otherwise `false`.
	 */
	public var isActive = false;
	/**
	 * The `RadioGroup` this radio button belongs to.
	 */
	public var group: RadioGroup;

	var innerMargin: Int = 0;

	var isHovered = false;
	var isClicked = false;

	/**
	 * Creates a new `RadioButton`.
	 * @param group The `RadioGroup` this radio button belongs to.
	 * @param label The label of this radio button.
	 */
	public function new(group: RadioGroup, label: String) {
		super();

		this.group = group;
		this.group.add(this);
		this.label = label;
	}

	override function onTIDChange() {
		width = p("width");
		height = p("height");

		innerMargin = Std.int((height - p("radio_size")) / 2);
	}

	#if !KOUI_EVENTS_OFF
	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated: isHovered = true;
			case Active:
			case Deactivated, Cancelled: isHovered = false;
		}
	}

	override function _onClick(event: Event) {
		if (event.mouseButton != Left) return;

		switch (event.state) {
			case Activated: isClicked = true;
			case Active:
			case Deactivated:
				isClicked = false;
				group.setActiveButton(this);
			case Cancelled:
				isClicked = false;
		}
	}
	#end

	public override function draw(g: kha.graphics2.Graphics) {
		var radio_size: Int = p("radio_size");

		if (p("texture") != null) {
			innerMargin = Std.int((drawHeight - p("atlas_height")) / 2);
		}

		var atlasOffsetX = isActive ? 1 : 0;
		var atlasOffsetY = isClicked ? 2 : (isHovered ? 1 : 0);

		if (!TextureAtlas.drawFromAtlas(g, this, 2, 3, atlasOffsetX, atlasOffsetY, drawX + innerMargin, drawY + innerMargin)) {
			g.color = p("color_bg");

			if (isActive) {
				if (isClicked) g.color = p("color_sec_clicked");
				else if (isHovered) g.color = p("color_sec_hover");
				else g.color = p("color_sec");
			} else {
				if (isClicked) g.color = p("color_clicked");
				else if (isHovered) g.color = p("color_hover");
			}

			g.fillCircle(drawX + innerMargin + radio_size / 2, drawY + drawHeight / 2, radio_size / 2);
		}

		g.color = p("color_text");
		g.font = ThemeUtil.getFont();
		g.fontSize = p("font_size");

		var offsetLeft = innerMargin * 2 + radio_size;
		g.drawAlignedString(label, drawX + offsetLeft, drawY + drawHeight / 2, TextLeft, TextMiddle);

		Border.draw(g, this);
	}
}
