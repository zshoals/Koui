package koui.elements.layouts;

/**
 * This (empty) interface must be implemented by layouts who have their own
 * sub-coordinate system.
 */
@:allow(koui.elements.Element)
@:keep
interface ICoordinateSystem {
}
