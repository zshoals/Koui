package koui.elements.layouts;

import kha.Image;
import kha.graphics2.Graphics;

import koui.effects.BoxShadow;
import koui.events.EventHandler;
import koui.events.Events;
import koui.utils.MathUtil;

/**
 * A scroll pane.
 */
class ScrollPane extends Layout implements ICoordinateSystem {
	/**
	 * The amount of pixels that are scrolled if the mouse wheel is rotated.
	 */
	public static final SCROLL_SENSITIVITY = 50;

	var elements: Array<Element> = new Array();
	var scrollX: kha.FastFloat = 0.0;
	var scrollY: kha.FastFloat = 0.0;

	var contentHeight = 0.0;
	var contentWidth = 0.0;

	var scrollTexture: Image;
	var sG: Graphics;

	var scrollbarWidth = 16;
	// -1 = no scrollbar
	var scrollbarLengthRight = -1;
	var scrollbarLengthBottom = -1;
	var isHovered = false;
	// Which scrollbar is currently clicked
	var clicked: Scrollbar = None;
	// Positions relative to the scrollbar that the user clicked
	var clickOffsetRight = 0;
	var clickOffsetBottom = 0;

	public function new(posX: Int, posY: Int, width: Int, height: Int) {
		super(posX, posY, width, height);

		createNewScrollTexture();
	}

	inline function createNewScrollTexture() {
		scrollTexture = Image.createRenderTarget(Std.int(width), Std.int(height), null, NoDepthAndStencil, 1, 0);
		sG = scrollTexture.g2;
	}

	override function onTIDChange() {
		this.scrollbarWidth = p("scrollbar_width");
	}

	/**
	 * Adds an element to the scroll pane.
	 * @param element The element
	 */
	public function add(element: Element) {
		element.layout = this;
		elements.push(element);

		// ScrollPane does not support anchors (better: it always uses the
		// TopLeft anchor) because it would not work to guess the position for
		// the anchor before knowing the content size of the ScrollPane
		element.layoutX += marginLeft;
		element.layoutY += marginTop;

		// Only scale to the right and down, otherwise we would have to
		// calculate an offset for the elements in this pane.
		// Also, do not take margin[Top/Left] into account here because they're
		// already calculated in layout[X/Y]
		if (element.layoutX + element.layoutWidth + marginRight > contentWidth) {
			contentWidth = element.layoutX + element.layoutWidth + marginRight;
		}
		if (element.layoutY + element.layoutHeight + marginBottom > contentHeight) {
			contentHeight = element.layoutY + element.layoutHeight + marginBottom;
		}

		// Ensure that the child elements of the element are also repositioned
		// if the element is a layout
		Layout.resizeIfSubLayout(element);

		recalulcateScrollbarLengths();
	}

	function recalulcateScrollbarLengths() {
		if (contentHeight > height)
			// Ratio * height
			scrollbarLengthRight = Std.int(height / contentHeight * height);
		if (contentWidth > width)
			scrollbarLengthBottom = Std.int(width / contentWidth * width);
	}

	public function remove(element: Element) {
		elements.remove(element);
	}

	public override function resize(width: Int, height: Int) {
		super.resize(width, height);

		createNewScrollTexture();
		recalulcateScrollbarLengths();
	}

	override function getAllElements(): Iterable<Element> {
		return elements;
	}

	#if !KOUI_EVENTS_OFF
	override function _onScroll(event: Event) {
		// Hovered over the bottom scroll bar
		if (scrollbarLengthBottom != -1 && EventHandler.mouseY > drawY + drawHeight - scrollbarWidth) {
			scrollX += event.scrollDelta * SCROLL_SENSITIVITY;
			scrollX = MathUtil.clamp(scrollX, 0, contentWidth - width);
		} else {
			scrollY += event.scrollDelta * SCROLL_SENSITIVITY;
			scrollY = MathUtil.clamp(scrollY, 0, contentHeight - height);
		}
	}

	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated, Active:
				isHovered = true;
			case Deactivated, Cancelled:
				isHovered = false;
		}
	}

	override function _onClick(event: Event) {
		switch (event.state) {
			case Activated:
				if (scrollbarLengthRight != -1 && EventHandler.mouseX >= drawX + width
						&& EventHandler.mouseY < drawY + height) {
					clicked = Right;

					var scrollOffset = scrollY / contentHeight * height;
					clickOffsetRight = Std.int(EventHandler.mouseY - posY - scrollOffset);
				}

				if (scrollbarLengthBottom != -1 && EventHandler.mouseY >= drawY + height
						&& EventHandler.mouseX < drawX + width) {
					clicked = Bottom;

					var scrollOffset = scrollX / contentWidth * width;
					clickOffsetBottom = Std.int(EventHandler.mouseX - posX - scrollOffset);
				}

				EventHandler.block(this);

			case Active:
				if (clicked == Right) {
					scrollY = MathUtil.mapToRange(
						EventHandler.mouseY - clickOffsetRight,
						posY, posY + height - scrollbarLengthRight,
						0, contentHeight - height);
					scrollY = MathUtil.clamp(scrollY, 0, contentHeight - height);
				}

				if (clicked == Bottom) {
					scrollX = MathUtil.mapToRange(
						EventHandler.mouseX - clickOffsetBottom,
						posX, posX + width - scrollbarLengthBottom,
						0, contentWidth - width);
					scrollX = MathUtil.clamp(scrollX, 0, contentWidth - width);
				}

			case Deactivated, Cancelled:
				clicked = None;
				EventHandler.unblock();
		}
	}
	#end

	override function draw(g: Graphics) {
		g.end();
		sG.begin(true, p("color_bg"));

		sG.pushTranslation(-scrollX, -scrollY);
		#if !(KOUI_EFFECTS_OFF || KOUI_EFFECTS_SHADOW_OFF)
		BoxShadow.drawOffsetX = -scrollX;
		BoxShadow.drawOffsetY = -scrollY;
		BoxShadow.drawSizeX = Std.int(this.drawWidth - scrollbarWidth);
		BoxShadow.drawSizeY = Std.int(this.drawHeight - scrollbarWidth);
		var oldG4 = Koui.g4;
		Koui.g4 = scrollTexture.g4;
		#end

		// Overlays are not drawn in the scroll pane to prevent clipping
		for (element in elements) {
			if (!element.visible) continue;
			g.opacity = element.p("opacity");
			element.draw(sG);
		}

		sG.popTransformation();
		sG.end();
		#if !(KOUI_EFFECTS_OFF || KOUI_EFFECTS_SHADOW_OFF)
		Koui.g4 = oldG4;
		BoxShadow.drawOffsetX = 0;
		BoxShadow.drawOffsetY = 0;
		BoxShadow.drawSizeX = -1;
		BoxShadow.drawSizeY = -1;
		#end

		g.begin(false);
		g.color = 0xffffffff;
		g.opacity = 1;
		g.drawImage(scrollTexture, drawX, drawY);

		drawScrollbar(g);

		#if KOUI_DEBUG_LAYOUT
		g.color = 0xffff0000;
		g.drawLine(drawX, drawY, drawX + width, drawY);
		g.drawLine(drawX, drawY + height, drawX + width, drawY + height);
		g.drawLine(drawX, drawY, drawX, drawY + height);
		g.drawLine(drawX + width, drawY, drawX + width, drawY + height);
		#end
	}

	/**
	 * Draws the scrollbar of this pane. Currently, the scrollbar is only
	 * outside of the pane.
	 * @param g The `kha.graphics2.Graphics` object for drawing
	 */
	function drawScrollbar(g: Graphics) {
		if (scrollbarLengthRight != -1) {
			drawWidth = width + scrollbarWidth;

			g.color = p("scrollbar_color_bg");
			g.fillRect(drawX + width, drawY, scrollbarWidth, height);

			g.color = p("scrollbar_color_front");
			if (clicked == Right) g.color = p("scrollbar_color_front_clicked");
			else if (isHovered) g.color = p("scrollbar_color_front_hover");

			var scrollRatio = scrollY / contentHeight;
			g.fillRect(drawX + width, drawY + scrollRatio * height, scrollbarWidth, scrollbarLengthRight);
		}

		if (scrollbarLengthBottom != -1) {
			drawHeight = height + scrollbarWidth;

			g.color = p("scrollbar_color_bg");
			g.fillRect(drawX, drawY + height, width, scrollbarWidth);

			g.color = p("scrollbar_color_front");
			if (clicked == Bottom) g.color = p("scrollbar_color_front_clicked");
			else if (isHovered) g.color = p("scrollbar_color_front_hover");

			var scrollRatio = scrollX / contentWidth;
			g.fillRect(drawX + scrollRatio * width, drawY + height, scrollbarLengthBottom, scrollbarWidth);
		}

		if (scrollbarLengthRight != -1 && scrollbarLengthBottom != -1) {
			g.color = p("scrollbar_color_corner");
			g.fillRect(drawX + width, drawY + height, scrollbarWidth, scrollbarWidth);
		}
	}

	/**
	 * Returns the topmost element at the given position. If no element exists
	 * at that position, `null` is returned.
	 *
	 * @param x The position's x coordinate
	 * @param y The position's y coordinate
	 * @return The element at the given position, `this` if not found but the
	 *         the mouse is over the scrollpane or `null` if the mouse is not
	 *         over the scrollpane.
	 */
	public override function getElementAtPosition(x: Int, y: Int): Null<Element> {
		// If the mouse is not over this scrollpane, don't check the contained
		// elements and return `null`.
		if (!MathUtil.hitbox(x, y, drawX, drawY, width, height)) {
			// Check scrollbars
			if (scrollbarLengthRight != -1) {
				if (MathUtil.hitbox(x, y, drawX + width, drawY, scrollbarWidth, height))
					return this;
			}

			if (scrollbarLengthBottom != -1) {
				if (MathUtil.hitbox(x, y, drawX, drawY + height, width, scrollbarWidth))
					return this;
			}

			return null;
		}

		if (receiveEvents) return this;

		// Calculate relative position
		x = Std.int(x - drawX + scrollX);
		y = Std.int(y - drawY + scrollY);

		// Reverse to ensure that the topmost element is selected
		var sorted_elements = elements.copy();
		sorted_elements.reverse();

		for (element in sorted_elements) {
			if (Std.is(element, Layout)) {
				var hit = cast(element, Layout).getElementAtPosition(x, y);
				if (hit != null) return hit;

				continue;
			}

			if (element.isAtPosition(x, y)) return element;
		}

		return this;
	}
}

private enum abstract Scrollbar(Int) {
	var None = 0;
	var Right = 1;
	var Bottom = 2;
}
