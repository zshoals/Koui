package koui.elements.layouts;

import koui.elements.layouts.Layout;
import koui.utils.MathUtil;

class AnchorPane extends Layout {
	var elements: Array<Element> = new Array();

	public function new(posX: Int, posY: Int, width: Int, height: Int) {
		super(posX, posY, width, height);
	}

	public function add(element: Element, anchor: Anchor = TopLeft) {
		element.anchor = anchor;
		element.layout = this;
		elements.push(element);

		recalcElement(element);
	};

	public function remove(element: Element) {
		elements.remove(element);
	}

	override function draw(g: kha.graphics2.Graphics) {
		for (element in elements) {
			if (!element.visible) continue;
			g.opacity = element.p("opacity");
			element.draw(g);
		}

		#if KOUI_DEBUG_LAYOUT
		g.color = 0xffff00ff;
		g.drawLine(drawX, drawY, drawX + drawWidth, drawY);
		g.drawLine(drawX, drawY + drawHeight, drawX + drawWidth, drawY + drawHeight);
		g.drawLine(drawX, drawY, drawX, drawY + drawHeight);
		g.drawLine(drawX + drawWidth, drawY, drawX + drawWidth, drawY + drawHeight);
		#end
	};

	public override function resize(width: Int, height: Int) {
		super.resize(width, height);

		for (element in elements) {
			recalcElement(element);
		}
	}

	override function getAllElements(): Iterable<Element> {
		return elements;
	}

	public override function getElementAtPosition(x: Int, y: Int): Null<Element> {
		// If the mouse is not over this scrollpane, don't check the contained
		// elements and return `null`.
		if (!this.isAtPosition(x, y)) return null;

		if (receiveEvents) return this;

		// Reverse to ensure that the topmost element is selected
		var sorted_elements = elements.copy();
		sorted_elements.reverse();

		for (element in sorted_elements) {
			if (Std.is(element, Layout)) {
				var hit = cast(element, Layout).getElementAtPosition(x, y);
				if (hit != null) return hit;

				continue;
			}

			if (element.isAtPosition(x, y)) return element;
		}

		return null;
	}

	function recalcElement(element: Element) {
		calcElementSize(element, this.layoutWidth, this.layoutHeight);

		switch (element.anchor) {
			case TopLeft:
				element.layoutX = this.layoutX + element.posX + marginLeft;
				element.layoutY = this.layoutY + element.posY + marginTop;
			case TopCenter:
				element.layoutX = Std.int(this.layoutX + this.layoutWidth / 2 - element.layoutWidth / 2 + element.posX);
				element.layoutY = this.layoutY + element.posY + marginTop;
			case TopRight:
				element.layoutX = this.layoutX + this.layoutWidth - element.layoutWidth + element.posX - marginRight;
				element.layoutY = this.layoutY + element.posY + marginTop;
			case MiddleLeft:
				element.layoutX = this.layoutX + element.posX + marginLeft;
				element.layoutY = Std.int(this.layoutY + this.layoutHeight / 2 - element.layoutHeight / 2 + element.posY);
			case MiddleCenter:
				element.layoutX = Std.int(this.layoutX + this.layoutWidth / 2 - element.layoutWidth / 2 + element.posX);
				element.layoutY = Std.int(this.layoutY + this.layoutHeight / 2 - element.layoutHeight / 2 + element.posY);
			case MiddleRight:
				element.layoutX = this.layoutX + this.layoutWidth - element.layoutWidth + element.posX -  marginRight;
				element.layoutY = Std.int(this.layoutY + this.layoutHeight / 2 - element.layoutHeight / 2 + element.posY);
			case BottomLeft:
				element.layoutX = this.layoutX + element.posX + marginLeft;
				element.layoutY = this.layoutY + this.layoutHeight - element.layoutHeight + element.posY - marginBottom;
			case BottomCenter:
				element.layoutX = Std.int(this.layoutX + this.layoutWidth / 2 - element.layoutWidth / 2 + element.posX);
				element.layoutY = this.layoutY + this.layoutHeight - element.layoutHeight + element.posY - marginBottom;
			case BottomRight:
				element.layoutX = this.layoutX + this.layoutWidth - element.layoutWidth + element.posX -  marginRight;
				element.layoutY = this.layoutY + this.layoutHeight - element.layoutHeight + element.posY - marginBottom;
		}

		// Ensure that the child elements of the element are also repositioned
		// if the element is a layout
		Layout.resizeIfSubLayout(element);
	}
}
