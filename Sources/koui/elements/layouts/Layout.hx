package koui.elements.layouts;

import koui.events.Events.Event;
import koui.utils.Log;

class Layout extends Element {
	public var marginLeft = 0;
	public var marginRight = 0;
	public var marginTop = 0;
	public var marginBottom = 0;

	/**
	 * If `true`, this layout does receive events.
	 *
	 * @see `spreadEvents`
	 */
	public var receiveEvents = false;

	/**
	 * If `true`, **all** elements of this layout will receive the same events
	 * as the layout itself. Individual events are not received then.
	 *
	 * This behaviour takes effect only if `receiveEvents` is `true`.
	 */
	public var spreadEvents = false;

	public function new(posX: Int, posY: Int, width: Int, height: Int) {
		super(posX, posY, width, height);
	}

	public override function onTIDChange() {
		this.marginLeft = p("margin_left");
		this.marginRight = p("margin_right");
		this.marginTop = p("margin_top");
		this.marginBottom = p("margin_bottom");
	}

	public function resize(width: Int, height: Int) {
		this.layoutWidth = width;
		this.layoutHeight = height;
	}

	public static function resizeIfSubLayout(element: Element) {
		if (Std.is(element, Layout)) {
			cast(element, Layout).resize(element.layoutWidth, element.layoutHeight);
		}
	}

	override function draw(g: kha.graphics2.Graphics) {
		throw Log.error("draw() function must be overriden by layout!");
	}

	public function getElementAtPosition(x: Int, y: Int): Null<Element> {
		throw Log.error("getElementAtPosition() function must be overriden by layout!");
		return null;
	}

	public function calcElementSize(element: Element, targetWidth: Int, targetHeight: Int) {
		var minWidth = element.p("width_min");
		var maxWidth = element.p("width_max");
		var minHeight = element.p("height_min");
		var maxHeight = element.p("height_max");

		if (minWidth != null || maxWidth != null) {
			element.layoutWidth = targetWidth - marginLeft - marginRight;

			switch(element.anchor) {
				case TopLeft, TopCenter, MiddleLeft, MiddleCenter, BottomLeft, BottomCenter:
					element.layoutWidth -= Std.int(Math.abs(element.posX));
				case TopRight, MiddleRight, BottomRight:
					element.layoutWidth += Std.int(Math.abs(element.posX));
			}

			if (minWidth != null && element.layoutWidth < minWidth) element.layoutWidth = minWidth;
			if (maxWidth != null && element.layoutWidth > maxWidth) element.layoutWidth = maxWidth;
		}

		if (minHeight != null || maxHeight != null) {
			element.layoutHeight = targetHeight - marginTop - marginBottom;

			switch(element.anchor) {
				case TopLeft, TopCenter, TopRight, MiddleLeft, MiddleCenter, MiddleRight:
					element.layoutHeight -= Std.int(Math.abs(element.posY));
				case BottomLeft, BottomCenter, BottomRight:
					element.layoutHeight += Std.int(Math.abs(element.posY));
			}

			if (minHeight != null && element.layoutHeight < minHeight) element.layoutHeight = minHeight;
			if (maxHeight != null && element.layoutHeight > maxHeight) element.layoutHeight = maxHeight;
		}

		Layout.resizeIfSubLayout(element);
	}

	/**
	 * Sets the margin values of this layout.
	 * @param left The left margin
	 * @param right The right margin
	 * @param top The top margin
	 * @param bottom The bottom margin
	 */
	public inline function setMargin(left: Int, right: Int, top: Int, bottom: Int) {
		this.marginLeft = left;
		this.marginRight = right;
		this.marginTop = top;
		this.marginBottom = bottom;
	}

	/**
	 * Sets the left margin of this layout.
	 * @param marginLeft The left margin
	 */
	public inline function setMarginLeft(left: Int) {
		this.marginLeft = left;
	}

	/**
	 * Sets the right margin of this layout.
	 * @param marginRight The right margin
	 */
	public inline function setMarginRight(right: Int) {
		this.marginRight = right;
	}

	/**
	 * Sets the top margin of this layout.
	 * @param marginTop The top margin
	 */
	public inline function setMarginTop(top: Int) {
		this.marginTop = top;
	}

	/**
	 * Sets the bottom margin of this layout.
	 * @param marginBottom The bottom margin
	 */
	public inline function setMarginBottom(bottom: Int) {
		this.marginBottom = bottom;
	}

	override function _onHover(event: Event) {
		if (!spreadEvents) return;
		for(e in getAllElements()) {
			if (e != null) e._onHover(event);
		}
	}
	override function _onClick(event: Event) {
		if (!spreadEvents) return;
		for(e in getAllElements()) {
			if (e != null) e._onClick(event);
		}
	}
	override function _onScroll(event: Event) {
		if (!spreadEvents) return;
		for(e in getAllElements()) {
			if (e != null) e._onScroll(event);
		}
	}
	override function _onKeyCharPress(event: Event) {
		if (!spreadEvents) return;
		for(e in getAllElements()) {
			if (e != null) e._onKeyCharPress(event);
		}
	}
	override function _onKeyCodePress(event: Event) {
		if (!spreadEvents) return;
		for(e in getAllElements()) {
			if (e != null) e._onKeyCodePress(event);
		}
	}
	override function _onKeyCodeStatus(event: Event) {
		if (!spreadEvents) return;
		for(e in getAllElements()) {
			if (e != null) e._onKeyCodeStatus(event);
		}
	}
	override function _onFocus(event: Event) {
		if (!spreadEvents) return;
		for(e in getAllElements()) {
			if (e != null) e._onFocus(event);
		}
	}

	function getAllElements(): Iterable<Element> {
		Log.error("getAllElements() must be overriden by subclass of layout!");
		return null;
	}
}

enum abstract Anchor(Int) to Int {
	var TopLeft;
	var TopCenter;
	var TopRight;
	var MiddleLeft;
	var MiddleCenter;
	var MiddleRight;
	var BottomLeft;
	var BottomCenter;
	var BottomRight;
}
