package koui.elements;

import koui.effects.Border;
import koui.events.EventHandler;
import koui.events.Events;
import koui.utils.TextureAtlas;
import koui.utils.ThemeUtil;

using kha.graphics2.GraphicsExtension;

/**
 * A `Dropdown` menu is a list of options from which the user can choose exactly
 * one option. It works like a group of `RadioButton`s but takes up less space.
 * A dropdown menu also has a label that describes the set of options.
 *
 * ```haxe
 * // Construct a new Dropdown with the label "DropdownLabel"
 * var myDropdown = new Dropdown("DropdownLabel");
 *
 * // Add some options
 * myDropdown.addOption("Option 1");
 * myDropdown.addOption("Option 2");
 * myDropdown.addOption("Option 3");
 *
 * // Select the first option
 * myDropdown.setSelectedOption("Option 1");
 *
 * // Add the dropdown to the scene
 * Koui.add(myDropdown);
 * ```
 */
class Dropdown extends Element {
	/**
	 * The label of this element.
	 */
	public var label = "";
	/**
	 * The currently selected option.
	 */
	public var selectedOption = "";
	/**
	 * All options of this dropdown menu.
	 */
	public var options: Array<String> = new Array();

	// -2 = not hovered, -1 = top hovered, 0-n = options hovered
	var hovered = -2;
	var isClicked = false;
	var isActive = false;

	public function new(label: String) {
		super();
		this.label = label;
	}

	override function onTIDChange() {
		if (p("texture") == null) {
			width = p("width");
			height = p("height");
		} else {
			if (p("atlas")) {
				width = p("atlas_width");
				height = p("atlas_height");
			} else {
				width = kha.Assets.images.get(p("texture")).width;
				height = Std.int(kha.Assets.images.get(p("texture")).height / 6);
			}
		}
	}

	/**
	 * Adds an option to the set of options.
	 * @param option The new option
	 */
	public function addOption(option: String) {
		options.push(option);
		if (options.length == 1) selectedOption = option;
	}

	/**
	 * Removes an option from the set of options.
	 * @param option The new option
	 */
	public function removeOption(option: String) {
		options.remove(option);
		if (options.length == 1) selectedOption = option;
	}

	/**
	 * Sets the currently selected option.
	 * @param option The option that should be selected
	 */
	public function setSelectedOption(option: String) {
		if (options.indexOf(option) == -1) {
			trace('Koui warning: Dropdown option $option does not exist!');
			return;
		}
		selectedOption = option;
	}

	/**
	 * Close the dropdown menu.
	 */
	public function close() {
		if (hovered > -1) calculateHoverPos();
		isActive = false;

		// Correct border drawing
		drawHeight = height;
		drawWidth = width;

		EventHandler.clearFocus();
		EventHandler.unblock();
		Koui.unregisterOverlay(this);
	}

	/**
	 * Calculates the correct value of the `hovered` variable.
	 */
	function calculateHoverPos() {
		var relMouseY = EventHandler.mouseY - drawY + getLayoutOffset()[1];
		var rowHeight: Int = height + p("separator_size");
		hovered = Std.int(relMouseY / rowHeight) - 1;
	}

	public override function draw(g: kha.graphics2.Graphics) {
		var atlasOffsetY = isClicked && hovered == -1 ? 2 : (hovered == -1 ? 1 : 0);

		if (!TextureAtlas.drawFromAtlas(g, this, 2, 6, 0, atlasOffsetY)) {
			g.color = p("color_bg");
			if (!isActive && hovered == -1) {
				g.color = isClicked ? p("color_clicked") : p("color_hover");
			}
			g.fillRect(drawX, drawY, drawWidth, height);
		}

		var margin: Null<Int> = p("margin");
		var arrow_size: Null<Int> = p("arrow_size");

		if (arrow_size != null || arrow_size != 0) {
			g.color = p("color_arrow");
			if (p("arrow_right")) {
				if (isActive) g.pushRotation(-Math.PI / 2, drawX + drawWidth - margin - arrow_size / 2, drawY + height / 2);
				g.fillTriangle(
					drawX + drawWidth - margin, drawY + height / 2 - arrow_size / 2,
					drawX + drawWidth - margin, drawY + height / 2 + arrow_size / 2,
					drawX + drawWidth - margin - arrow_size, drawY + height / 2);
			} else {
				if (isActive) g.pushRotation(Math.PI / 2, drawX + margin + arrow_size / 2, drawY + height / 2);
				g.fillTriangle(
					drawX + margin, drawY + height / 2 - arrow_size / 2,
					drawX + margin, drawY + height / 2 + arrow_size / 2,
					drawX + margin + arrow_size, drawY + height / 2);
			}
			if (isActive) g.popTransformation();
		}

		var drawText = isActive || options.length == 0 ? label : selectedOption;
		g.fontSize = p("font_size");
		g.font = ThemeUtil.getFont();
		g.color = p("color_text");

		if (p("arrow_right")) {
			g.drawAlignedString(drawText, drawX + margin, drawY + height / 2, TextLeft, TextMiddle);
		} else {
			g.drawAlignedString(drawText, drawX + margin * 2 + arrow_size, drawY + height / 2, TextLeft, TextMiddle);
		}

		if (disabled) {
			g.color = p("color_disabled");
			g.fillRect(drawX, drawY, drawWidth, height);
		}

		Border.draw(g, this);
	}

	public override function drawOverlay(g: kha.graphics2.Graphics) {
		this.drawHeight = this.height;

		if (isActive) {
			for (i in 0...options.length) {
				var option = options[i];

				var optionY = drawY + p("separator_size") * (i + 1) + height * (i + 1);

				var atlasOffsetY = 0;
				if (option == selectedOption) {
					atlasOffsetY = 3;
					if (hovered == i) {
						atlasOffsetY = isClicked ? 5 : 4;
					}
				}
				if (hovered == i) {
					atlasOffsetY = isClicked ? 2 : 1;
				}

				if (!TextureAtlas.drawFromAtlas(g, this, 2, 6, 1, atlasOffsetY, drawX, optionY)) {
					// Draw option background
					if (option == selectedOption) {
						g.color = p("color_sec");
						if (hovered == i) {
							g.color = isClicked ? p("color_sec_clicked") : p("color_sec_hover");
						}
					} else {
						g.color = p("color_bg");
						if (hovered == i) {
							g.color = isClicked ? p("color_clicked") : p("color_hover");
						}
					}

					g.fillRect(drawX, optionY, drawWidth, height);
				}
				this.drawHeight += height;

				// Draw separator
				g.color = p("color_separator");
				g.fillRect(drawX, drawY + drawHeight - height, drawWidth, p("separator_size"));
				this.drawHeight += p("separator_size");

				g.color = option == selectedOption ? p("color_text_selected") : p("color_text");
				g.fontSize = p("font_size");
				g.font = ThemeUtil.getFont();
				g.drawAlignedString(option, drawX + p("margin"), drawY + drawHeight - height / 2, TextLeft, TextMiddle);
			}
		}

		Border.draw(g, this);
	}

	#if !KOUI_EVENTS_OFF
	override function _onHover(event: Event) {
		switch (event.state) {
			case Activated, Active:
				if (event.mouseMoved) calculateHoverPos();
			case Deactivated, Cancelled:
				hovered = -2;
		}
	}

	override function _onClick(event: Event) {
		switch (event.state) {
			case Activated:
				switch(event.mouseButton) {
					case Left:
						if (isActive && hovered == -1) close();
						else isClicked = true;
					case Right: close();
					default:
				}
			case Active:
			case Deactivated, Cancelled:
				isClicked = false;
				if (hovered > -1) {
					if (hovered < options.length) {
						selectedOption = options[hovered];
					}
					close();
				}
		}
	}

	override function _onFocus(event: Event) {
		if (event.state == Activated) {
			isActive = true;
			EventHandler.block(this);
			Koui.registerOverlay(this);
		} else if (event.state == Deactivated) {
			close();
		}
	}

	override function _onKeyCodeStatus(event: Event) {
		if (event.state == Activated) {
			switch (event.keyCode) {
				case Escape:
					close();
				case Up:
					if (hovered < 0) hovered = options.length;
					if (hovered > 0) hovered--;
				case Down:
					if (hovered < 0) hovered = -1;
					if (hovered < options.length - 1) hovered++;
				case Return:
					selectedOption = options[hovered];
					close();
				default:
			}
		}
	}

	override function _onScroll(event: Event) {
		// Same behaviour as arrow up/down keys
		if (event.scrollDelta < 0) {
			if (hovered < 0) hovered = options.length;
			if (hovered > 0) hovered--;
		} else if (event.scrollDelta > 0) {
			if (hovered < 0) hovered = -1;
			if (hovered < options.length - 1) hovered++;
		}
	}
	#end
}
