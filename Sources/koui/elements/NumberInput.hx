package koui.elements;

/**
 * A number input.
 */
class NumberInput extends TextInput {
	/**
	 * The regular expression used to check whether a string is a valid
	 * **signed** floating point number. Note that the decimal separator is a
	 * decimal point(`.`) as it is common in the English language.
	 *
	 * It also accepts strings beginning with a `+` or `-` sign.
	 *
	 * @see [`REG_UNSIGNED_FLOAT`](#REG_UNSIGNED_FLOAT)
	 */
	public static final REG_FLOAT = ~/^[\-\+]?[0-9]+(\.[0-9]+)?$/gm;
	/**
	 * The regular expression used to check whether a string is a valid
	 * **signed**
	 * integer number.
	 *
	 * It also accepts strings beginning with a `+` or `-` sign.
	 *
	 * @see [`REG_UNSIGNED_INT`](#REG_UNSIGNED_INT)
	 */
	public static final REG_INT = ~/^[\-\+]?[0-9]+$/gm;

	/**
	 * The regular expression used to check whether a string is a valid
	 * **unsigned** floating point number. Note that the decimal separator is a
	 * decimal point(`.`) as it is common in the English language.
	 *
	 * It also accepts strings beginning with a `+` sign.
	 *
	 * @see [`REG_INT`](#REG_INT)
	 */
	public static final REG_UNSIGNED_FLOAT = ~/^[\+?[0-9]+(\.[0-9]+)?$/gm;

	/**
	 * The regular expression used to check whether a string is a valid
	 * **unsigned** integer number.
	 *
	 * It also accepts strings beginning with a `+` sign.
	 *
	 * @see [`REG_FLOAT`](#REG_FLOAT)
	 */
	public static final REG_UNSIGNED_INT = ~/^\+?[0-9]+$/gm;

	/**
	 * The number type that is accepted by this `NumberInput`.
	 * @see `setInputType()`
	 */
	public var inputType(default, null) = NumberType.TypeFloat;

	/**
	 * Creates a new `NumberInput`.
	 *
	 * @param inputType The input type of the new element
	 * @param text The label of the new element
	 */
	public function new(inputType: NumberType, label: String = "") {
		super(label);
		setInputType(inputType);
	}

	/**
	 * Sets the input type of this element. Do not use `inputType`
	 * directly, it is read-only.
	 *
	 * @param inputType The new input type of this element
	 */
	public function setInputType(inputType: NumberType) {
		this.inputType = inputType;

		this.validationReg = switch (inputType) {
			case TypeFloat: REG_FLOAT;
			case TypeUnsignedFloat: REG_UNSIGNED_FLOAT;
			case TypeInt: REG_INT;
			case TypeUnsignedInt: REG_UNSIGNED_INT;
		}
	}

	/**
	 * Sets the floating point value of this element. If this number input
	 * accepts integer vaues only, the decimal places of the given value are
	 * truncated towards zero. If the given value is not a finite number
	 * (`Math.NaN`, `Math.POSIIVE_INIFINITY` etc.) or `null` (possible on
	 * non-static targets only), nothing will happen.
	 *
	 * @param value The new value
	 */
	public function setFloatValue(value: Null<Float>) {
		if (!Math.isFinite(value) || value == null) return;
		if (inputType == TypeInt || inputType == TypeUnsignedInt) {
			value = (value > 0) ? Math.ffloor(value) : Math.fceil(value);
		}
		this.text = Std.string(value);
	}

	/**
	 * Returns the floating point value of this element. If the input is
	 * invalid, `Math.NaN` gets returned. This function is equivalent to
	 * `getFloatValueOrDefault(Math.NaN)`.
	 *
	 * @return Float
	 * @see [`getFloatValueOrDefault()`](#getFloatValueOrDefault)
	 */
	public function getFloatValue(): Float {
		if (!valid) return Math.NaN;
		return Std.parseFloat(text);
	}

	/**
	 * Returns the floating point value of this element. If the input is
	 * invalid, the given default value gets returned.
	 *
	 * @param defaultValue The default value (default: 0.0)
	 * @return Float
	 * @see [`getFloatValue()`](#getFloatValue)
	 */
	public function getFloatValueOrDefault(defaultValue: Float = 0.0): Float {
		if (!valid) return defaultValue;
		return Std.parseFloat(text);
	}

	/**
	 * Sets the integer value of this element. If the given value is `null`
	 * (possible on non-static targets only), nothing will happen.
	 *
	 * @param value The new value
	 */
	public function setIntValue(value: Null<Int>) {
		if (value == null) return;
		this.text = Std.string(value);
	}

	/**
	 * Returns the integer value of this element. If the input is invalid,
	 * `null` gets returned (make sure to test this in your code!).
	 *
	 * @return Int
	 * @see [`getIntValueOrDefault()`](#getIntValueOrDefault)
	 */
	public function getIntValue(): Null<Int> {
		if (!valid) return null;
		return Std.parseInt(text);
	}

	/**
	 * Returns the integer value of this element. If the input is invalid, the
	 * given default value gets returned.
	 *
	 * @param defaultValue The default value (default: 0)
	 * @return Int
	 * @see [`getIntValue()`](#getIntValue)
	 */
	public function getIntValueOrDefault(defaultValue: Int = 0): Int {
		if (!valid) return defaultValue;
		return Std.parseInt(text);
	}
}

/**
 * Represents a number type used to describe what numbers `NumberInput` accepts.
 */
enum abstract NumberType(Int) {
	/**
	 * Represents a signed floating point number.
	 */
	var TypeFloat;

	/**
	 * Represents an unsigned floating point number.
	 */
	var TypeUnsignedFloat;

	/**
	 * Represents a signed integer number.
	 */
	var TypeInt;

	/**
	 * Represents an unsigned integer number.
	 */
	var TypeUnsignedInt;
}
