package koui;

import haxe.ds.Vector;

import kha.graphics2.Graphics;
import kha.Framebuffer;
import kha.Image;

import koui.Theme;
import koui.effects.Effect;
import koui.elements.Element;
import koui.elements.layouts.AnchorPane;
import koui.elements.layouts.Layout.Anchor;
import koui.events.EventHandler;
import koui.utils.Cursor;
import koui.utils.MathUtil;
import koui.utils.SceneManager;

/**
 * Koui's main class.
 */
class Koui {

	/**
	 * The theme's root `kha.Font` object (for initialization of components).
	 * Used internally only.
	 */
	public static var font(default, null): kha.Font;
	/**
	 * The theme's root font size (for initialization of components).
	 * Used internally only.
	 */
	public static var fontSize(default, null): Int;

	/**
	 * The default layout. Gets initialized in `init()`.
	 */
	static var anchorPane: AnchorPane;
	static var overlays: Array<Element> = new Array();
	// Two arrays to compensate for the not existing ordered map data type
	static var overlay_offsets: Array<Vector<Int>> = new Array();

	static var initialized = false;

	static var lastScreenW = 0;
	static var lastScreenH = 0;

	@:allow(koui.elements.Element)
	static var textPipeline: kha.graphics4.PipelineState;

	public static var g4: kha.graphics4.Graphics;

	/**
	 * Initializes Koui. Call this in your application's initialization method.
	 * @param font
	 */
	public static function init() {
		Koui.font = kha.Assets.fonts.get(koui.Theme.theme["_root"]["font"]);
		Koui.fontSize = koui.Theme.theme["_root"]["font_size"];

		var textVS = kha.graphics4.Graphics2.createTextVertexStructure();
		textPipeline = kha.graphics4.Graphics2.createTextPipeline(textVS);
		textPipeline.alphaBlendSource = SourceAlpha;
		textPipeline.alphaBlendDestination = InverseSourceAlpha;
		textPipeline.blendSource = SourceAlpha;
		textPipeline.blendDestination = InverseSourceAlpha;
		textPipeline.blendOperation = Add;
		textPipeline.compile();

		var windowId = 0;
		var screenWidth = kha.Window.get(windowId).width;
		var screenHeight = kha.Window.get(windowId).height;
		var antialiasing = 1;

		lastScreenW = screenWidth;
		lastScreenH = screenWidth;

		anchorPane = new AnchorPane(0, 0, screenWidth, screenHeight);

		var assets = [
			"cursor_text",
			"cursor_notallowed",
			"theme_default.json",
			"Montserrat_Bold",
			"Montserrat_Italic",
			"Montserrat_Regular"
		];
		kha.Assets.loadEverything(function() {
			Cursor.init();
			Effect.initAll();

			// EventHandler calls setCursor(), so it must be initialized after
			// the Cursor class
			EventHandler.init();

			initialized = true;
			kha.System.notifyOnFrames(render);
		}, function(asset: Dynamic): Bool {
			if (assets.indexOf(asset.name) != -1)
				return true;
			return false;
		});
	}

	/**
	 * Adds an element to be drawn.
	 * @param element The element
	 * @param anchor The anchor position of the new element.
	 */
	public static inline function add(element: Element, anchor: Anchor = TopLeft) {
		anchorPane.add(element, anchor);
	}

	/**
	 * Removes an element from the drawing list.
	 * @param element The element
	 */
	public static inline function remove(element: Element) {
		anchorPane.remove(element);
	}

	/**
	 * Sets the margin values of the default `AnchorPane`.
	 * @param left The left margin
	 * @param right The right margin
	 * @param top The top margin
	 * @param bottom The bottom margin
	 */
	public static inline function setMargin(left: Int, right: Int, top: Int, bottom: Int) {
		anchorPane.setMargin(left, right, top, bottom);
	}

	/**
	 * Sets the left margin of the default `AnchorPane`.
	 * @param marginLeft The left margin
	 */
	public static inline function setMarginLeft(left: Int) { anchorPane.setMarginLeft(left); }

	/**
	 * Sets the right margin of the default `AnchorPane`.
	 * @param marginRight The right margin
	 */
	public static inline function setMarginRight(right: Int) { anchorPane.setMarginRight(right); }

	/**
	 * Sets the top margin of the default `AnchorPane`.
	 * @param marginTop The top margin
	 */
	public static inline function setMarginTop(top: Int) { anchorPane.setMarginTop(top); }

	/**
	 * Sets the bottom margin of the default `AnchorPane`.
	 * @param marginBottom The bottom margin
	 */
	public static inline function setMarginBottom(bottom: Int) { anchorPane.setMarginBottom(bottom); }

	public static inline function registerOverlay(element: Element) {
		overlays.push(element);
		// Static overlay position for now
		overlay_offsets.push(element.getLayoutOffset());
	}

	public static inline function unregisterOverlay(element: Element) {
		var pos = overlays.indexOf(element);
		overlays.remove(element);
		overlay_offsets.remove(overlay_offsets[pos]);
	}

	/**
	 * Main drawing method responsible for drawing everything. Call this in you
	 * render loop.
	 * @param g The `kha.graphics2.Graphics` object for drawing
	 */
	public static function render(framebuffers: Array<Framebuffer>) {
		if (!initialized) return;

		#if KOUI_DEBUG_DRAWINGTIME
		var t1 = kha.Scheduler.realTime();
		#end

		checkScreenChanged();

		g4 = framebuffers[0].g4;
		var g2 = framebuffers[0].g2;

		// Default values if nothing is set via theme, to at least draw text
		g2.fontSize = fontSize;
		g2.font = font;
		g2.color = 0xffffffff;

		#if !KOUI_EVENTS_OFF
		EventHandler.update();
		#end

		g2.begin(false);
		anchorPane.draw(g2);
		for (i in 0...overlays.length) {
			var offset = overlay_offsets[i];
			g2.pushTranslation(-offset[0], -offset[1]);

			overlays[i].drawOverlay(g2);

			g2.popTransformation();
		}

		#if !KOUI_EVENTS_OFF
		EventHandler.reset();
		#end

		Cursor.draw(g2);

		#if KOUI_DEBUG_DRAWINGTIME
		g2.color = 0xffff0000;
		g2.font = font;
		g2.fontSize = 16;
		g2.drawString('Drawing time: ${kha.Scheduler.realTime() - t1}', 0, 0);
		#end

		g2.end();
	}

	/**
	 * Returns the topmost element at the given position. If no element exists
	 * at that position, `null` is returned.
	 *
	 * @param x The position's x coordinate
	 * @param y The position's y coordinate
	 * @return The element at the given position or `null` if not found
	 */
	public static function getElementAtPosition(x: Int, y: Int): Null<Element> {
		// Check overlayed elements first
		var sorted_overlays = overlays.copy();

		// Reverse to ensure that the topmost element is selected
		sorted_overlays.reverse();

		for (i in 0...sorted_overlays.length) {
			var element = sorted_overlays[i];
			var offset = overlay_offsets[i];

			var relX = x + offset[0];
			var relY = y + offset[1];

			if (element.isAtPosition(relX, relY)) return element;
		}

		// If no overlay was hit, check the other objects
		return anchorPane.getElementAtPosition(x, y);
	}

	static function checkScreenChanged() {
		var screenW = kha.Window.get(0).width;
		var screenH = kha.Window.get(0).height;

		if (screenW != lastScreenW || screenH != lastScreenH) onResize(screenW, screenH);

		lastScreenW = screenW;
		lastScreenH = screenH;
	}

	static function onResize(width: Int, height: Int) {
		anchorPane.resize(width, height);
		SceneManager.resizeAll(width, height);
	}
}
