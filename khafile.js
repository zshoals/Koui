let project = new Project("Koui");

project.addAssets("Assets/cursors/cursor_text.png");
project.addAssets("Assets/cursors/cursor_notallowed.png");
project.addAssets("Assets/theme_default.json");
project.addAssets("Assets/Montserrat-Bold.ttf");
project.addAssets("Assets/Montserrat-Italic.ttf");
project.addAssets("Assets/Montserrat-Regular.ttf");
project.addSources("Sources");
project.addShaders("Shaders/**");

resolve(project);
