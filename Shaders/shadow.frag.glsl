#version 450

#ifdef GL_ES
precision mediump float;
#endif

uniform float boxLeft;
uniform float boxRight;
uniform float boxTop;
uniform float boxBottom;
uniform float radius;
uniform float falloff;
uniform vec4 color;

out vec4 fragColor;

float distanceToRect(float left, float right, float top, float bottom, vec2 p) {
    float dx = max(0, max(left - p.x, p.x - right));
    float dy = max(0, max(bottom - p.y, p.y - top));
    return sqrt(dx*dx + dy*dy);
}

void main() {
    float dst = distanceToRect(boxLeft, boxRight, boxTop, boxBottom, gl_FragCoord.xy);
    // Invert, clamp and falloff
    dst = pow(max(0, (1 - dst / radius)), falloff);

    fragColor = vec4(color.r, color.g, color.b, dst * color.a);
}
