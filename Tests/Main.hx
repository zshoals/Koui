package;

import utest.Runner;
import utest.ui.Report;

// import elements.*;

class Main {
	static function main() {
		// Required for runner.run(). Don't know exactly why... It looks like
		// you need it because runner.run() uses haxe.Timer() and creating
		// a new kha window with kha.System.start() does not work because of the
		// node target, so it needs to be initialized manually.
		kha.Scheduler.init();

		var runner = new Runner();
		// runner.addCase(new TestTextInput());
		// runner.addCase(new TestNumberInput());

		Report.create(runner);
		runner.run();
	}
}
